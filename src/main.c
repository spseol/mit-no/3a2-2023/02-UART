#include "main.h"
#include "daughterboard.h"
#include "milis.h"
#include <stdbool.h>
#include <stdio.h>
#include <stm8s.h>

uint32_t time = 0;
unsigned char bagr;
unsigned char string[33] = "12345678901234567";
unsigned char tmp_string[33];
uint8_t index = 0;
uint8_t k = 0;

void init(void) {
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // taktovani MCU na 16MHz

    GPIO_Init(LED1_PORT, LED1_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(S1_PORT, S1_PIN, GPIO_MODE_IN_FL_NO_IT);

    init_milis();

    // UART
    UART1_DeInit();
    UART1_Init(9600,
               UART1_WORDLENGTH_8D,
               UART1_STOPBITS_1,
               UART1_PARITY_NO,
               UART1_SYNCMODE_CLOCK_DISABLE,
               UART1_MODE_TXRX_ENABLE
    );

    enableInterrupts(); // globálně povolí přerušovací systém
    UART1_ITConfig(UART1_IT_RXNE, ENABLE); // povolí přerušení při příjmu znaku
    
}

int putchar(int c) 
{
    while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET)
        ;
    UART1_SendData8(c);
    return c;
}


void rx_action(void)
{
    REVERSE(LED1);
    // vlajka zůstává nahoře dokud není příchozí znak přečten
    // při přečtení příchozích dat vlajak samam spadne
    bagr = UART1_ReceiveData8();
    //putchar(bagr);
    
    if (bagr == '\n') {
        // obsaha tmp_string nakopíruju do string
        k = 0;
        do {
            string[k] = tmp_string[k];
        } while (string[k++]);
        index = 0;
        tmp_string[0] = '\0';
    } else if (bagr == '\r') {
        ;    
    } else {
        tmp_string[index] = bagr;
        tmp_string[index+1] = '\0';
        index++;
    }
}

int main(void) {

    init();

    while (1) {
        
        // if (UART1_GetFlagStatus(UART1_FLAG_RXNE)) {
        //     rx_action();
        // }

        if (milis() - time > 2222) {
            time = milis();

            // putchar('A');
            // putchar('h');
            // putchar('\r');
            // putchar('\n');
            printf("\norig: >%s<\n", string);
            printf("tmp:  >%s<\n\n", tmp_string);
        }
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"
